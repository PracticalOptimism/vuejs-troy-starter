
import { PROJECT } from '../../../@utility/constants/project'

import Vue from 'vue'
import index from '../data-structures/index.vue'
import '../algorithms/register-service-worker'

import { vueRouter } from '../../../provider/service/vue-router/variables/web-browser-location-router'
import { vuexStore } from '../../../provider/service/vuex/variables/distributed-ledger-technology'

Vue.config.productionTip = false

new Vue({
  router: vueRouter,
  store: vuexStore,
  render: (h) => h(index)
}).$mount(`#${PROJECT.projectId}`)
