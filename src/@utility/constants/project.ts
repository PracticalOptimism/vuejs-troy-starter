
const PROJECT = {
  'projectId': 'app',
  'projectNameDescription': '',
  'projectOrganizationId': '',
  'projectOrganizationNameDescription': '',
  'projectNameIdentifierGroup': {},
  'projectWebsiteIdentifierGroup': {},

  'textDescriptionGroup': {}
}

export {
  PROJECT
}
