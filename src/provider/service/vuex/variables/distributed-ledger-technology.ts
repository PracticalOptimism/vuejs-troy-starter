import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const vuexStore = new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})

export {
  vuexStore
}
