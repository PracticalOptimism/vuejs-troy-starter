import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import HomePage from '../../../../usecase/home-page/data-structures/home-page.vue'
import AboutPage from '../../../../usecase/about-page/data-structures/about-page.vue'

Vue.use(VueRouter)

const routes: RouteConfig[] = [
  { path: '/', name: 'Home', component: HomePage },
  { path: '/about', name: 'About', component: AboutPage }
]

const vueRouter = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export {
  vueRouter
}
